# Use the PHP 7.2 FPM Alpine image as the base image
FROM php:7.2-fpm-alpine

# Set the working directory in the container
WORKDIR /var/www/html

# Install dependencies
RUN apk --update add \
    libzip-dev \
    unzip \
    && docker-php-ext-install zip pdo_mysql

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Copy the application files to the container
COPY . /var/www/html

# Install Laravel dependencies
RUN composer install --no-interaction --optimize-autoloader

# Expose port 9000 (PHP-FPM)
EXPOSE 9000

# Start PHP-FPM
CMD ["php-fpm"]
